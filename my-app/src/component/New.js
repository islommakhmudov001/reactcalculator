import React, {useState} from 'react';

function New() {
  const [number, setNumber] = useState("");
  const [item , setItem] = useState(false)
  const [res, setRes] = useState();
  const handler = e => {
    setNumber(e.target.value);
  }

  return (
    <div className="w-[100%] bg-black h-[100vh] grid place-items-center">
      <div className=" text-center">
        <input
          className="placeholder:italic placeholder:text-slate-400 block bg-white w-full  rounded-3xl py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 text-5xl"
          type="text"
          value={number}
          name={number}
          onChange={handler}/>
        <h4 className="m-2 text-4xl font-bold text-amber-50 font-sans">Result: {res}</h4>
        <div className="m-2">
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '1')}>1
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '2')}>2
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '3')}>3
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '4')}>4
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '5')}>5
          </button>
        </div>
        <div className="m-2">
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '6')}>6
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '7')}>7
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '8')}>8
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '9')}>9
          </button>
          <button className=" w-28 h-28 bg-amber-500 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '0')}>0
          </button>
        </div>
        <div className="m-2">
          <button className=" w-28 h-28 bg-blue-400 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '+')}>+
          </button>
          <button className=" w-28 h-28 bg-blue-400 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '-')}>-
          </button>
          <button className=" w-28 h-28 bg-blue-400 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '*')}>*
          </button>
          <button className=" w-28 h-28 bg-blue-400 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber(number + '/ ')}>%
          </button>
        </div>
        <div className="m-2">
          <button className=" w-28 h-28 bg-green-700 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setRes(eval(number))}>=
          </button>
          <button className=" w-28 h-28 bg-green-700 text-amber-50 text-4xl rounded-full ml-2 font-bold" onClick={() => setNumber('')}>C</button>
        </div>
      </div>
    </div>
  );
}

export default New;

